# task 1

square <- function(x){
  x^2
}
cosinus <- function(s1,s2){
  multiplication <- sum(s1 * s2)
  s1_module <- sqrt(sum(sapply(s1, square)))
  s2_module <- sqrt(sum(sapply(s2, square)))
  
  result <- multiplication / (s1_module * s2_module)
  result
}

cosinus(c(1,2,4,5), c(2,1,7,4))

# task 2
l1 <- replicate(30, rnorm(10), simplify = F)
l1

# task 3
names(l1) <- paste('vector', 1:30, sep='')
l1

# task 4
m1 = simplify2array(lapply(l1, FUN = function(x) lapply(l1, FUN = function(y) cosinus(x,y))))
m1

# task 5
diag(m1) <- NA
cosinus_mean = apply(m1, 1, function(x) mean(simplify2array(x), na.rm=TRUE))
cosinus_mean

# task 6
two_max_values = apply(m1, 1, function(x) simplify2array(x)[order(simplify2array(x), decreasing = TRUE)][1:2])
two_max_values

# task 7
lapply(apply(m1, 1, function(x) which(x > 0.5)), names)